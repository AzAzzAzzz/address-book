<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class ContactsTableSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++)
        {
            DB::table('contacts')->insert([
                'name'       => $faker->firstNameMale,
                'last_name'  => $faker->lastName,
                'patronymic' => $faker->firstNameFemale,
                'group_id'   => $faker->numberBetween($min = 0, $max = 2),
                'phone'      => '+7 (555) 888-88-88',
                'home_phone' => '+7 (989) 66-66-66',
                'email'      => $faker->email,
                'post'       => 'developer',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
