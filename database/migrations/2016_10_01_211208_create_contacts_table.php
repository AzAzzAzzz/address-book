<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable()->comment('Имя контакта');
            $table->string('last_name', 50)->nullable()->comment('Фамилия контакта');
            $table->string('patronymic', 50)->nullable()->comment('Отчество контакта');
            $table->integer('group_id')->nullable()->comment('ИД группы к которой добавлен контакт');
            $table->string('phone', 30)->nullable()->comment('моб., тел. контакта');
            $table->string('home_phone', 30)->nullable()->comment('домашний тел. контакта');
            $table->string('email', 50)->nullable()->comment('e-mail адрес контакта');
            $table->string('post', 30)->nullable()->comment('Должность контакта');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
