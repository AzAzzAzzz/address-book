$(function () {
    $("#phone").mask("+ 7 (999) 999-99-99");
    $("#home_phone").mask("+ 7 (999) 99-99-99");
});

/**
 * Валидация формы Добавления контакта
 */
$('#add-address-form').validate({
    submitHandler: function (form) {
        var data = $(form).serialize();

        showLoading();

        $.ajax({
            url: 'add/contact',
            type: 'POST',
            data: data
        }).done(function (response) {
            hideLoading();
            if (response.is_error === true) {
                $('#error-result')
                    .empty()
                    .append(response.msg)
                    .removeClass('hidden');

                return false;
            }

            $('#success-result')
                .empty()
                .append(response.msg)
                .removeClass('hidden');

            $('#add-address-form')[0].reset();
        });
        return false;
    },
    rules: {
        name: {
            required: true,
            minlength: 4
        },
        last_name: {
            required: true,
            minlength: 4
        },
        patronymic: {
            required: true,
            minlength: 4
        },
        phone: {
            required: true
        },
        email: {
            email: true
        }
    },
    messages: {
        name: {
            required: 'Пожалуйста, введите Имя.',
            minlength: 'Поле (Имя) должно содержать не меннее 4 символов.'
        },
        last_name: {
            required: 'Пожалуйста, введите Фамилию.',
            minlength: 'Поле (Фамилия) должно содержать не меннее 4 символов.'
        },
        patronymic: {
            required: 'Пожалуйста, введите Отчество.',
            minlength: 'Поле (Отчество) должно содержать не меннее 4 символов.'
        },
        phone: {
            required: 'Пожалуйста, введите номер телефона.'
        },
        email: {
            email: 'Пожалуйста, введиет корректный е-mail адрес.'
        }
    }
});