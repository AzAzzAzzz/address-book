$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    /**
     * Удаление контакта
     */
    $('.remove-contact').on('click', function () {
        var contact = $(this).parents('tr'),
            contactName = contact.find('td.name').text(),
            contactId = $(this).data('contact-id');

        $.confirm({
            title: 'Подтвердите действие',
            content: 'Удалить контакт <strong>'+contactName+'</strong> из адресной книги?',
            confirmButton: 'Да',
            cancelButton: 'Нет',
            confirm: function() {
                $.ajax({
                     url: 'contacts/delete',
                     method: 'POST',
                     data: {
                         id: contactId
                     }
                }).done(function (response) {
                    if (response.is_error === true) {
                        $.alert(response.msg);
                    }
                    contact.remove();
                });
            },
            cancel: function(){
                return true;
            }
        });
    });

    /**
     * Редактирование данных контакта
     */
    $('body').on('click', '.edit-contact', function () {
        var id = $(this).data('contactId'),
            groupId = $(this).parents('tr').find('.group_id').val();

        $.ajax({
            url: 'contacts/get',
            method: 'POST',
            data: {
                id: id
            }
        }).done(function (response) {
            if (response.is_error === false) {
                $.dialog({
                    title: 'Редактировать контакт',
                    content: response.data.html,
                    columnClass: 'col-sm-9 col-sm-offset-1',
                    confirmButton: 'Да',
                    cancelButton: 'Нет',
                    onOpen: function () {
                        $('#group option[value="'+groupId+'"]').prop('selected', true);

                        $('#phone').mask("+ 7 (999) 999-99-99");
                        $('#home_phone').mask("+ 7 (999) 99-99-99");

                        validate();
                    }
                });
            }
        });
    });
});


/**
 * Валидация формы редактирования данных контакта
 */
function validate () {
    $('#edit-address-form').validate({
        submitHandler: function (form) {
            var data = $(form).serialize();

            save(data);

            return false;
        },
        rules: {
            name: {
                required: true,
                minlength: 4
            },
            last_name: {
                required: true,
                minlength: 4
            },
            patronymic: {
                required: true,
                minlength: 4
            },
            phone: {
                required: true
            },
            email: {
                email: true
            }
        },
        messages: {
            name: {
                required: 'Пожалуйста, введите Имя.',
                minlength: 'Поле (Имя) должно содержать не меннее 4 символов.'
            },
            last_name: {
                required: 'Пожалуйста, введите Фамилию.',
                minlength: 'Поле (Фамилия) должно содержать не меннее 4 символов.'
            },
            patronymic: {
                required: 'Пожалуйста, введите Отчество.',
                minlength: 'Поле (Отчество) должно содержать не меннее 4 символов.'
            },
            phone: {
                required: 'Пожалуйста, введите номер телефона.'
            },
            email: {
                email: 'Пожалуйста, введиет корректный е-mail адрес.'
            }
        }
    });
}

/**
 * Сохранение измененных данных контакта
 */
function save (data) {
    showLoading();

    $.ajax({
        url: 'contacts/update',
        type: 'POST',
        data: data
    }).done(function (response) {
        hideLoading();
        if (response.is_error === true) {
            $('#error-result')
                .empty()
                .append(response.msg)
                .removeClass('hidden');
            return false;
        }

        contactUpdate(response.data.contact);

        $('#success-result')
            .empty()
            .append(response.msg)
            .removeClass('hidden');
    });
}

/**
 * Обновление данных контакта в таблице контактов
 */
function contactUpdate (data) {
    var contact = $('#contact-'+data.id+'');

    $.each(data, function (index, val) {
        if (index !== 'group_id') {
            $('#contact-'+data.id+' .'+index+'').text(val);
        } else {
            $('#contact-'+data.id+' .'+index+'').val(val);
        }
    });
}