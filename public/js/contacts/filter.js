var filter = $('#filter');

/**
 * Фильтрация таблицы по столбцу "Имя"
 */
filter.keyup(function(){
    $('td.name').each( function(){
        if(filter.val().toLowerCase() != $(this).text().toLowerCase().substr(0, filter.val().length)) {
            $(this)
                .parent()
                .hide();
        } else {
            $(this)
                .parent()
                .show();
        }
    });
});