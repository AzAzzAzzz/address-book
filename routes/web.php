<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/', function () {
    return view('home.index');
});

Route::get('/add/', 'AddController@index');
// Добавление нового контакта
Route::post('/add/contact', 'AddController@contact');
// Список контактов адресной книги
Route::get('/contacts', 'ContactsController@index');
// Удаление контакта
Route::post('/contacts/delete', 'ContactsController@delete');
// Получение формы редактирования контакта
Route::post('/contacts/get', 'ContactsController@getForm');
// Обновление данных контакта
Route::post('/contacts/update', 'ContactsController@update');
