<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    protected $result = [
        'is_error' => false,
        'msg'      => null,
        'data'     => null,
    ];

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
