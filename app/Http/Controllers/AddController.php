<?php

namespace App\Http\Controllers;

use Request;
use Validator;

use App\Models\Contacts;
use App\Models\ContactsGroup;
use Illuminate\Support\Facades\Lang;

/**
 * Класс для добаввление новых данных в адресную книгу
 *
 * @package App\Http\Controllers
 */
class AddController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        return view('add.index', [
            'title'     => 'Новый контакт',
            'pageTitle' => 'Добавление контакта',
            'groups'    => ContactsGroup::all()
        ]);
    }

    /**
     * Получение данных контакта от клиента,
     * и создание нового конткта после успешной валидации
     *
     * @return mixed - JSON {
     *      'is_error': 'Принимает false, если данные были успешно сохранены',
     *      'msg': 'Информационное сообщение пользователю',
     *      'data': 'Данные для передачи на клиетскую сторону',
     * }
     */
    public function contact ()
    {
        $this->validation();

        $result = $this->create();

        if ($result) {
            $this->result['msg'] = Lang::get('contacts.success_adding');
            return response()->json($this->result);
        }

        $this->result['msg'] = Lang::get('contacts.error_adding');
        return response()->json($result);
    }

    /**
     * Валидация данных нового контака
     *
     * @return bool|mixed @return mixed - JSON {
     *      'is_error': 'Принимает true, если не удалось провалидировать данные контакта',
     *      'msg': 'Информационное сообщение пользователю',
     *      'data': 'Данные для передачи на клиетскую сторону',
     * }
     */
    private function validation () {
        $validator = Validator::make(Request::all(), [
            'name'       => 'required|min:4',
            'last_name'  => 'required|min:4',
            'patronymic' => 'required|min:4',
            'phone'      => 'required',
            'email'      => 'email'
        ]);

        if ($validator->fails())
        {
            $this->result['is_error'] = true;
            $this->result['data']     = $validator->messages();

            return response()->json($this->result);
        }

        return true;
    }

    /**
     * Создание нового контакта
     *
     * @return bool - Принимает true, если данные контакта успешно сохранены
     */
    private function create () {
        $contacts = new Contacts;

        $contacts->group_id   = Request::input('group');
        $contacts->name       = Request::input('name');
        $contacts->last_name  = Request::input('last_name');
        $contacts->patronymic = Request::input('patronymic');
        $contacts->phone      = Request::input('phone');
        $contacts->home_phone = Request::input('home_phone');
        $contacts->email      = Request::input('email');
        $contacts->post       = Request::input('post');

        return $contacts->save();
    }

}
