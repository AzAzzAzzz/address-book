<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Contacts;
use Illuminate\Support\Facades\Lang;
use App\Models\ContactsGroup;

/**
 * Класс управления контактами
 *
 * @package App\Http\Controllers
 */
class ContactsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        return view('contacts.index', [
            'title'     => 'Адресная книга',
            'pageTitle' => 'Список контактов',
            'contacts'  => Contacts::all(),
            'groups'    => ContactsGroup::all()
        ]);
    }

    /**
     * Возвращает форму радактирования данных контакта по его ИД
     *
     * @return mixed - JSON {
     *      'is_error': '',
     *      'msg': 'Информационное сообщение пользователю',
     *      'data': {
     *          'html': 'Шаблон формы редактирования, с данными контакта'
     *      },
     * }
     */
    public function getForm ()
    {
        $id = Request::input('id');

        $tpl = view('contacts.edit_form', [
            'groups'  => ContactsGroup::all(),
            'contact' => Contacts::find($id)
        ]);

        $this->result['data'] = ['html' => $tpl->render()];
        return response()->json($this->result);
    }

    /**
     * Удаление контакта
     *
     * @return mixed - - JSON {
     *      'is_error': 'Принимает false, если данные были успешно удалены',
     *      'msg': 'Информационное сообщение пользователю',
     *      'data': 'Данные для передачи на клиетскую сторону',
     * }
     */
    public function delete ()
    {
        $id = Request::input('id');

        $result = Contacts::destroy($id);

        if ($result > 0) {
            $this->result['msg'] = Lang::get('contacts.success_delete');
            return response()->json($this->result);
        }

        $this->result['msg'] = Lang::get('contacts.error_delete');
        return response()->json($this->result);
    }


    /**
     * Обновление данных контакта
     *
     * @return mixed - JSON {
     *      'is_error': 'Принимает false, если данные были успешно обновлены',
     *      'msg': 'Информационное сообщение пользователю',
     *      'data': {
     *          'contact': {'Обновленные данные контакта'}
     *      }
     * }
     */
    public function update () {
        $contact = Contacts::find(Request::input('id'));

        $contact->group_id   = Request::input('group');
        $contact->name       = Request::input('name');
        $contact->last_name  = Request::input('last_name');
        $contact->patronymic = Request::input('patronymic');
        $contact->phone      = Request::input('phone');
        $contact->home_phone = Request::input('home_phone');
        $contact->email      = Request::input('email');
        $contact->post       = Request::input('post');

        $result = $contact->save();

        if ($result) {
            $this->result['msg'] = Lang::get('contacts.success_update');
            $this->result['data'] = ['contact' => $contact];
            return response()->json($this->result);
        }

        $this->result['msg'] = Lang::get('contacts.error_update');
        return response()->json($this->result);
    }
}
