@extends('main')

@section('content')
    <h1 class="title">{{ $pageTitle }}</h1>

    <div id="error-result" class="alert alert-danger hidden" role="alert"></div>

    <div id="success-result" class="alert alert-success hidden" role="alert"></div>

    <form action="" method="POST" id="add-address-form">
        <input type="hidden" name="_token" value="{{csrf_token()}}" >
        <p class="help-info text-center">
            <span class="required">*</span> - Помечены поля обязательные для заполнения
        </p>
        <div class="form-group row">
            <label for="address-group" class="col-xs-3 col-form-label text-right">Группа:</label>
            <div class="col-xs-9">
                <select class="form-control" name="group">
                    <option value="0">выберите группу</option>
                    @foreach ($groups as $group)
                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-xs-3 col-form-label text-right">Имя: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="name" id="name" placeholder="Введите Имя...">
            </div>
        </div>
        <div class="form-group row">
            <label for="last_name" class="col-xs-3 col-form-label text-right">Фамилия: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="last_name" id="last_name" placeholder="Введите Фамилию...">
            </div>
        </div>
        <div class="form-group row">
            <label for="patronymic" class="col-xs-3 col-form-label text-right">Отчество: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="patronymic" id="patronymic" placeholder="Введите Отчество...">
            </div>
        </div>
        <div class="form-group row">
            <label for="phone" class="col-xs-3 col-form-label text-right">Мобильный телефон: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="phone" id="phone" placeholder="Введите номер...">
            </div>
        </div>
        <div class="form-group row">
            <label for="home_phone" class="col-xs-3 col-form-label text-right">Домашний телефон:</label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="home_phone" id="home_phone" placeholder="Введите номер...">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-xs-3 col-form-label text-right">E-mail:</label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="email" id="email" placeholder="Введите e-mail адрес...">
            </div>
        </div>
        <div class="form-group row">
            <label for="post" class="col-xs-3 col-form-label text-right">Должность:</label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="post" id="post" placeholder="Введите должность...">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-12">
                <button class="btn btn-primary pull-right">
                    <span class="glyphicon glyphicon-plus"></span>
                    Добавить
                </button>
            </div>
        </div>
    </form>

@push('scripts')
<script src="{{ asset('/public/js/lib/jquery/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('/public/js/lib/jquery/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/public/js/add/add.js') }}"></script>
@endpush

@stop