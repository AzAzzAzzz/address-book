<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }}</title>

    <meta name="_token" content="{!! csrf_token() !!}"/>

    <link href="{{ asset('/public/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/add/index.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/js/lib/jquery/jquery-confirm/css/jquery-confirm.min.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('/public/js/lib/jquery/jquery-1.12.2.min.js') }}"></script>
</head>

<body>
<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">AddressBook</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/contacts">Адресная книга</a>
                </li>
                <li>
                    <a href="/add">Добавить контакт</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="jumbotron col-sm-12">
    @yield('content')
    </div>

    <div id="preloader" class="hidden">
        <div>
            <img src="/public/images/preloaders/54-54.gif" alt="Загрузка...">
            <p>Загрузка...</p>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-center text-muted copy">&copy AddressBook</p>
    </div>
</div>
<script src="{{ asset('/public/js/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('/public/js/main.js') }}"></script>
@stack('scripts')

</body>
</html>