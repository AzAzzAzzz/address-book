<div class="form">
    <div class="alert alert-success hidden" id="success-result"></div>
    <div class="alert alert-danger hidden" id="error-result"></div>
    <form action="" method="POST" id="edit-address-form">
        <p class="help-info text-center">
            <span class="required">*</span> - Помечены поля обязательные для заполнения
        </p>
        <div class="form-group row">
            <label for="address-group" class="col-xs-3 col-form-label text-right">Группа:</label>
            <div class="col-xs-9">
                <select id="group" class="form-control" name="group">
                    <option value="0">выберите группу</option>
                    @foreach ($groups as $group)
                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-xs-3 col-form-label text-right">Имя: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="name" id="name" value="{{ $contact->name }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="last_name" class="col-xs-3 col-form-label text-right">Фамилия: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="last_name" id="last_name" value="{{ $contact->last_name }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="patronymic" class="col-xs-3 col-form-label text-right">Отчество: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="patronymic" id="patronymic" value="{{ $contact->patronymic }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="phone" class="col-xs-3 col-form-label text-right">Мобильный телефон: <span class="required">*</span></label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="phone" id="phone" value="{{ $contact->phone }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="home_phone" class="col-xs-3 col-form-label text-right">Домашний телефон:</label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="home_phone" id="home_phone" value="{{ $contact->home_phone }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-xs-3 col-form-label text-right">E-mail:</label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="email" id="email" value="{{ $contact->email }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="post" class="col-xs-3 col-form-label text-right">Должность:</label>
            <div class="col-xs-9">
                <input class="form-control" type="text" name="post" id="post" value="{{ $contact->post }}">
            </div>
        </div>
        <input class="form-control" type="hidden" name="id" value="{{ $contact->id }}">
        <div class="form-group row">
            <div class="col-sm-12">
                <button class="btn btn-success pull-right">
                    <span class="glyphicon glyphicon-floppy-saved"></span>
                    Сохранить
                </button>
            </div>
        </div>
    </form>
</div>