@extends('main')

@section('content')

    <h1 class="title">{{ $pageTitle }}</h1>
    <input type="hidden" name="_token" value="{{csrf_token()}}" >
    @if ( ! empty($contacts))
        <div class="filter-form">
            <div class="form-group">
                <input type="text" class="form-control" id="filter" placeholder="Фильтрация по Имени..">
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">Имя</th>
                <th class="text-center">Фамилия</th>
                <th class="text-center">Отчество</th>
                <th class="text-center">Телефон</th>
                <th class="text-center">E-mail</th>
                <th class="text-center">Должность</th>
                <th class="text-center">Действия</th>
            </tr>
            </thead>
            <tbody class="contact-list">
            @foreach ($contacts as $contact)
                <tr id="contact-{{ $contact->id }}">
                    <td class="name">{{ $contact->name }}</td>
                    <td class="last_name">{{ $contact->last_name }}</td>
                    <td class="patronymic">{{ $contact->patronymic }}</td>
                    <td>
                        <span class="glyphicon glyphicon-phone"></span>:
                        <span class="phone">{{ $contact->phone }}</span><br />
                        @if ( ! empty($contact->home_phone))
                        <span class="glyphicon glyphicon-phone-alt"></span>:
                        <span class="home_phone">{{ $contact->home_phone }}</span>
                        @endif
                    </td>
                    <td class="email">{{ $contact->email }}</td>
                    <td class="post">{{ $contact->post }}</td>
                    <td class="text-center">
                        <span class="remove-contact glyphicon glyphicon-trash" title="Удалить" data-contact-id="{{ $contact->id }}"></span>
                        <span class="edit-contact glyphicon glyphicon-pencil" title="Редактировать" data-contact-id="{{ $contact->id }}"></span>
                    </td>
                    <input class="group_id" type="hidden" value="{{ $contact->group_id }}" />
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
       <p class="text-center">Список контактов пуст.</p>
    @endif

@push('scripts')
<script src="{{ asset('/public/js/lib/jquery/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('/public/js/lib/jquery/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/public/js/lib/jquery/jquery-confirm/js/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('/public/js/contacts/contacts.js') }}"></script>
<script src="{{ asset('/public/js/contacts/filter.js') }}"></script>
@endpush

@stop